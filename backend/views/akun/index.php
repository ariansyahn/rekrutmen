<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AkunSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akun-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Tambah Admin HRD', ['admin-hrd-add'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Tambah Koordinator Unit', ['admin-koordinator-add'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'email:email',
            [
                'label' => 'Role',
                'value' => 'role.nama_role',
                'filter' => Html::activeDropDownList($searchModel, 'user_role', \backend\models\Role::getUserRoleList(),['class'=>'form-control','prompt' => 'All']),
            ],
            
            [
                'label' => 'Action',
                'format' => 'raw',
                'value' => function($model, $key, $index, $column) {
                    return Html::a('Lihat', ['view', 'id' => $model->id_akun], ['class' => 'btn btn-primary']);
                }
            ],
        ],
    ]); ?>
</div>
